# sql存储过程的变化



## 删除存储过程
    drop + 过程名

## 重命名存储过程名
     execute sp_rename 原存储过程名, 新存储过程名

## 修改存储过程语法
```sql
    alter proc proc_StudentInfo
    @code nvarchar(80) output
    as
    begin
        select @code
        select @code=StudentCode from StudentInfo where Id=1
        select @code
    end
```
## 自定函数之标量函数 语法
* 1、自定义函数的定义

    >SQL自定义函数：标量函数和表值函数 标量函数：标量函数是对单一值操作，返回单一值 表值函数：返回表类型的数据。

    >表值函数的返回类型是一个表，因此，可以像使用表一样使用表值函数。

* 2、自定义函数格式
  ```sql
    create function <方法名称>
    (
        [
            @XXXX 数据类型 [=默认值],
            @YYYY 数据类型 [=默认值]
        ]
    )
    returns int -- 表明返回会值是一个int的值
    as
    begin
        
        declare @result int

        set @result=1

        return @result

    end
  ```
* 3、自定义函数的使用

```sql
    create function fn_GetInt
    (

    )
    returns int -- 表明返回会值是一个int的值
    as
    begin
        
        declare @result int

        set @result=1

        return @result

    end

    go
    --调用自定义函数
    select dbo.fn_GetInt()
  ```



