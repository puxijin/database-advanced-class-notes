/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2021/9/22 19:11:19                           */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('vipinso') and o.name = 'FK_VIPINSO_REFERENCE_会员卡等级信息')
alter table vipinso
   drop constraint FK_VIPINSO_REFERENCE_会员卡等级信息
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('会员车辆信息表') and o.name = 'FK_会员车辆信息表_REFERENCE_VIPINSO')
alter table 会员车辆信息表
   drop constraint FK_会员车辆信息表_REFERENCE_VIPINSO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('商品信息表') and o.name = 'FK_商品信息表_REFERENCE_店铺信息表')
alter table 商品信息表
   drop constraint FK_商品信息表_REFERENCE_店铺信息表
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('店铺信息表') and o.name = 'FK_店铺信息表_REFERENCE_商品信息表')
alter table 店铺信息表
   drop constraint FK_店铺信息表_REFERENCE_商品信息表
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"Store information"')
            and   type = 'U')
   drop table "Store information"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('vipinso')
            and   type = 'U')
   drop table vipinso
go

if exists (select 1
            from  sysobjects
           where  id = object_id('会员卡积分规则信息')
            and   type = 'U')
   drop table 会员卡积分规则信息
go

if exists (select 1
            from  sysobjects
           where  id = object_id('会员卡等级信息')
            and   type = 'U')
   drop table 会员卡等级信息
go

if exists (select 1
            from  sysobjects
           where  id = object_id('会员积分兑换表')
            and   type = 'U')
   drop table 会员积分兑换表
go

if exists (select 1
            from  sysobjects
           where  id = object_id('会员车辆信息表')
            and   type = 'U')
   drop table 会员车辆信息表
go

if exists (select 1
            from  sysobjects
           where  id = object_id('商品信息表')
            and   type = 'U')
   drop table 商品信息表
go

if exists (select 1
            from  sysobjects
           where  id = object_id('店铺信息表')
            and   type = 'U')
   drop table 店铺信息表
go

/*==============================================================*/
/* Table: "Store information"                                   */
/*==============================================================*/
create table "Store information" (
   MallNumber           int                  not null,
   MallName             nvarchar(80)         null,
   MallSmill            nvarchar(80)         null,
   MallAdd              nvarchar(80)         null,
   phone                int                  null,
   constraint "PK_STORE INFORMATION" primary key (MallNumber)
)
go

/*==============================================================*/
/* Table: vipinso                                               */
/*==============================================================*/
create table vipinso (
   会员编号                 int                  not null,
   姓名                   nvarchar(80)         null,
   年龄                   nvarchar(80)         null,
   性别                   nvarchar(80)         null,
   联系方式                 int                  null,
   会员卡号                 int                  null,
   constraint PK_VIPINSO primary key (会员编号)
)
go

/*==============================================================*/
/* Table: 会员卡积分规则信息                                             */
/*==============================================================*/
create table 会员卡积分规则信息 (
   规则编号                 int                  not null,
   条款                   nvarchar(80)         null,
   宗旨                   nvarchar(80)         null,
   constraint PK_会员卡积分规则信息 primary key (规则编号)
)
go

/*==============================================================*/
/* Table: 会员卡等级信息                                               */
/*==============================================================*/
create table 会员卡等级信息 (
   卡号                   int                  null,
   等级                   nvarchar(80)         null,
   注册时间                 nvarchar(80)         null
)
go

/*==============================================================*/
/* Table: 会员积分兑换表                                               */
/*==============================================================*/
create table 会员积分兑换表 (
   积分兑换编号               int                  not null,
   会员编号                 int                  null,
   会员卡号                 int                  null,
   兑换记录                 nvarchar(80)         null,
   constraint PK_会员积分兑换表 primary key (积分兑换编号)
)
go

/*==============================================================*/
/* Table: 会员车辆信息表                                               */
/*==============================================================*/
create table 会员车辆信息表 (
   会员编号                 int                  null,
   会员车牌号                nvarchar(80)         null,
   会员卡等级                nvarchar(80)         null
)
go

/*==============================================================*/
/* Table: 商品信息表                                                 */
/*==============================================================*/
create table 商品信息表 (
   商品编号                 int                  not null,
   商品名称                 nvarchar(80)         null,
   单价                   money                null,
   数量                   int                  null,
   店铺编号                 int                  null,
   constraint PK_商品信息表 primary key (商品编号)
)
go

/*==============================================================*/
/* Table: 店铺信息表                                                 */
/*==============================================================*/
create table 店铺信息表 (
   店铺编号                 int                  not null,
   店铺名称                 nvarchar(80)         null,
   店铺位置                 nvarchar(80)         null,
   constraint PK_店铺信息表 primary key (店铺编号)
)
go

alter table vipinso
   add constraint FK_VIPINSO_REFERENCE_会员卡等级信息 foreign key (会员卡号)
      references 会员卡等级信息 (卡号)
go

alter table 会员车辆信息表
   add constraint FK_会员车辆信息表_REFERENCE_VIPINSO foreign key (会员编号)
      references vipinso (会员编号)
go

alter table 商品信息表
   add constraint FK_商品信息表_REFERENCE_店铺信息表 foreign key (店铺编号)
      references 店铺信息表 (店铺编号)
go

alter table 店铺信息表
   add constraint FK_店铺信息表_REFERENCE_商品信息表 foreign key (商品编号)
      references 商品信息表 (商品编号)
go

