/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2021/9/22 19:11:19                           */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('vipinso') and o.name = 'FK_VIPINSO_REFERENCE_会员卡等级信息')
alter table vipinso
   drop constraint FK_VIPINSO_REFERENCE_会员卡等级信息
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('会员车辆信息表') and o.name = 'FK_会员车辆信息表_REFERENCE_VIPINSO')
alter table 会员车辆信息表
   drop constraint FK_会员车辆信息表_REFERENCE_VIPINSO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('商品信息表') and o.name = 'FK_商品信息表_REFERENCE_店铺信息表')
alter table 商品信息表
   drop constraint FK_商品信息表_REFERENCE_店铺信息表
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('店铺信息表') and o.name = 'FK_店铺信息表_REFERENCE_商品信息表')
alter table 店铺信息表
   drop constraint FK_店铺信息表_REFERENCE_商品信息表
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"Store information"')
            and   type = 'U')
   drop table "Store information"
go

if exists (select 1
            from  sysobjects
           where  id = object_id('vipinso')
            and   type = 'U')
   drop table vipinso
go

if exists (select 1
            from  sysobjects
           where  id = object_id('会员卡积分规则信息')
            and   type = 'U')
   drop table 会员卡积分规则信息
go

if exists (select 1
            from  sysobjects
           where  id = object_id('会员卡等级信息')
            and   type = 'U')
   drop table 会员卡等级信息
go

if exists (select 1
            from  sysobjects
           where  id = object_id('会员积分兑换表')
            and   type = 'U')
   drop table 会员积分兑换表
go

if exists (select 1
            from  sysobjects
           where  id = object_id('会员车辆信息表')
            and   type = 'U')
   drop table 会员车辆信息表
go

if exists (select 1
            from  sysobjects
           where  id = object_id('商品信息表')
            and   type = 'U')
   drop table 商品信息表
go

if exists (select 1
            from  sysobjects
           where  id = object_id('店铺信息表')
            and   type = 'U')
   drop table 店铺信息表
go

/*==============================================================*/
/* Table: "Storeinformation"             商场信息表                      */
/*==============================================================*/
create table "Storeinformation" (
   MallNumber           int                  not null,
   MallName             nvarchar(80)         null,
   MallSmill            nvarchar(80)         null,
   MallAdd              nvarchar(80)         null,
   phone                int                  null,
   constraint "PK_STORE INFORMATION" primary key (MallNumber)
)
go
/*==============================================================*/
/* Table: UserInfo                   用户信息表                          */
/*==============================================================*/
create table UserInfo
(
	Id int not null identity primary key,
	Name nvarchar(10) not null unique,
	Password  nvarchar(10) not null 
)

/*==============================================================*/
/* Table: vipinfo      会员信息表                                         */
/*==============================================================*/
create table vipinfo (
   会员编号                 int                  not null,
   姓名                   nvarchar(80)         null,
   年龄                   nvarchar(80)         null,
   性别                   nvarchar(80)         null,
   联系方式                 int                  null,
   会员卡号                 int                  null,
   constraint PK_VIPINSO primary key (会员编号)
)
go

/*==============================================================*/
/* Table:  integrationRule     会员卡积分规则信息                                             */
/*==============================================================*/
create table integrationRule (
   规则编号                 int                  not null,
   条款                   nvarchar(80)         null,
   宗旨                   nvarchar(80)         null,
   constraint PK_会员卡积分规则信息 primary key (规则编号)
)
go

/*==============================================================*/
/* Table: 会员卡等级信息                                               */
/*==============================================================*/
create table vipintegral (
   卡号                   int                  null,
   等级                   nvarchar(80)         null,
   注册时间                 nvarchar(80)         null
)
go

/*==============================================================*/
/* Table: 会员积分兑换表                                               */
/*==============================================================*/
create table vipexchange (
   积分兑换编号               int                  not null,
   会员编号                 int                  null,
   会员卡号                 int                  null,
   兑换记录                 nvarchar(80)         null,
   constraint PK_会员积分兑换表 primary key (积分兑换编号)
)
go

/*==============================================================*/
/* Table: 会员车辆信息表                                               */
/*==============================================================*/
create table vipcarinfo (
   会员编号                 int                  null,
   会员车牌号                nvarchar(80)         null,
   会员卡等级                nvarchar(80)         null
)
go

/*==============================================================*/
/* Table: 商品信息表                                                 */
/*==============================================================*/
create table commodityinfo (
   商品编号                 int                  not null,
   商品名称                 nvarchar(80)         null,
   单价                   money                null,
   数量                   int                  null,
   店铺编号                 int                  null,
   constraint PK_商品信息表 primary key (商品编号)
)
go

/*==============================================================*/
/* Table: 店铺信息表                                                 */
/*==============================================================*/
create table storeinfo (
   shopid                 int                  not null,
   shopname                 nvarchar(80)         null,
   shopadd                 nvarchar(80)         null,
   constraint PK_店铺信息表 primary key (shopid)
)
go

--alter table vipinso
--   add constraint FK_VIPINSO_REFERENCE_会员卡等级信息 foreign key (会员卡号)
--      references 会员卡等级信息 (卡号)
--go

--alter table 会员车辆信息表
--   add constraint FK_会员车辆信息表_REFERENCE_VIPINSO foreign key (会员编号)
--      references vipinso (会员编号)
--go

--alter table 商品信息表
--   add constraint FK_商品信息表_REFERENCE_店铺信息表 foreign key (店铺编号)
--      references 店铺信息表 (店铺编号)
--go

--alter table 店铺信息表
--   add constraint FK_店铺信息表_REFERENCE_商品信息表 foreign key (商品编号)
--      references 商品信息表 (商品编号)
--go

--注册
insert into UserInfo (Name,Password)
values  ('admin','123')

--批量注册
insert into UserInfo (Name,Password)
values
('admin01','124'),('admin02','125'),('admin03','126');

--登录

--根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败
select * from UserInfo
where Name='admin' and Password='123'


--判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
--declare @username nvarchar(10)
--declare @tmpTable1 table (id int,Username nvarchar(80),Password nvarchar(80))
--declare @count1 int 

--insert into @tmpTable1
--select @count1 = count(*) from UserInfo where Name=@username

declare @username nvarchar(80)
declare @tmpTablel table (id int  ,Username nvarchar(80),Password nvarchar(80))
declare @couunt1 int 
insert into @tmpTablel
select @couunt1 = COUNT(*) from UserInfo where Name=@username

/*
if(@count1 >0)
	begin

	做点什么 如提示消息或者设置一些数值

	end 
 else 
	begin

	end
*/

---判断密码和重复密码是否一致，是否可以注册，并在相应数据表中插入一条记录，否则返回失败信息

--declare @password nvarchar(10)
--declare @tmpTable2 table (id int,Username nvarchar(80),Password nvarchar(80))
--declare @count2 int 

--insert into @tmpTable2
--select @count2 = count(*) from UserInfo where Password=@password


declare @password nvarchar(80)
declare @tmpTable2 table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count2 int 

insert into @tmpTable2
select @count2 =COUNT(*) from UserInfo where Password=@password

/*



if(@count >0)
	begin
insert into UserInfo (Name,Password)
values （' ' , ' '）

	end 

 else 
	begin

	end
*/

--关于商场信息的使用场景

----在会员信息管理的时候，需要选择商场
--select SMId,SMName,SMAbbreviation  from SMInfo
select MallNumber,MallName,MallSmill,MallAdd,phone from Storeinformation
----在会员卡类型管理的时候，需要选择商场
--select SMId,SMName,SMAbbreviation  from SMInfo
select MallNumber,MallName,MallSmill,MallAdd,phone from Storeinformation
----商场信息 的数据增删改查
--insert into SMInfo (SMName,SMAbbreviation,SMAddress,SMIps,IpsTel) values ( )
--delete from SMInfo where 
--update  SMInfo set   where 
--select * from SMInfo


--insert into Storeinformation (MallNumber,MallName,MallSmill,MallAdd,phone) values()
--delete from Storeinformation  where 
--update Storeinformation set where
select * from Storeinformation




----关于店铺信息的使用场景
----在积分管理的时候，需要选择店铺
--select ShopsId,ShopsName  from ShopsInfo
select shopid,shopname,shopadd from storeinfo

----店铺信息 的数据增删改查
--insert into ShopsInfo (ShopsName,SMId,ShopsPosition,ShopesOperator,OperatorTel) values ( )
--delete from ShopsInfo where 
--update  ShopsInfo  set   where 
--select * from ShopsInfo


--insert into storeinfo (ShopsName,MallNumber,MallAdd) values ( )
--delete from storeinfo where 
--update  storeinfo  set   where 
select * from storeinfo

